import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:planner/calendar.dart';
import 'work_form.dart';

class NoteWidget extends StatefulWidget {
  NoteWidget({Key? key}) : super(key: key);

  @override
  _NoteWidgetState createState() => _NoteWidgetState();
}

class _NoteWidgetState extends State<NoteWidget> {
  CollectionReference works = FirebaseFirestore.instance.collection('works');

  Future<void> deleteWork(workId) {
    return works
        .doc(workId)
        .delete()
        .then((value) => print("Work Deleted"))
        .catchError((error) => print("Failed to delete work: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("NOTE"),
        centerTitle: true,
        backgroundColor: Colors.blue,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () async {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(IconData(0xe3b3, fontFamily: 'MaterialIcons')),
          ),
        ],
      ),
      body: StreamBuilder(
        stream: works.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data!.docs[index];

                return Card(
                  margin: EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['topic']),
                    subtitle: Text(documentSnapshot['date']),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  WorkForm(workId: documentSnapshot.id)));
                    },
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () async {
                        await deleteWork(documentSnapshot.id);
                      },
                    ),
                  ),
                );
              },
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CalendarWidget()),
          );
        },
        backgroundColor: Colors.black,
        child: Icon(IconData(0xf00d, fontFamily: 'MaterialIcons'),
            color: Colors.white),
      ),
    );
  }
}
