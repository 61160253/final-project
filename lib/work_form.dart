import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:planner/note.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WorkForm extends StatefulWidget {
  String workId;
  WorkForm({Key? key, required this.workId}) : super(key: key);

  @override
  _WorkFormState createState() => _WorkFormState(this.workId);
}

class _WorkFormState extends State<WorkForm> {
  String workId;
  String date = " ";
  String topic = " ";
  String detail = " ";
  String dmy = "";
  String days = "";

  @override
  void initState() {
    super.initState();
    _loadCalendar1();
    if (this.workId.isNotEmpty) {
      works.doc(this.workId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            dmy = data['date'];
          });

          topic = data['topic'];
          detail = data['detail'];
          //  _dateController.text = date;
          _topicController.text = topic;
          _detailController.text = detail;
        }
      });
    }
  }

  Future<void> _loadCalendar1() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      dmy = prefs.getString('dmy') ?? '';
    });
  }

  Future<void> _saveCalendar1() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('dmy', dmy);
    });
  }

  CollectionReference works = FirebaseFirestore.instance.collection('works');
  _WorkFormState(this.workId);

  TextEditingController _dateController = new TextEditingController();
  TextEditingController _topicController = new TextEditingController();
  TextEditingController _detailController = new TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Future<void> addWork() {
    return works
        .add({'date': this.dmy, 'topic': this.topic, 'detail': this.detail})
        .then((value) => print("Work Added"))
        .catchError((error) => print("Failed to add work: $error"));
  }

  Future<void> updateWork() {
    return works
        .doc(this.workId)
        .update({'date': this.date, 'topic': this.topic, 'detail': this.detail})
        .then((value) => print("Work Updated"))
        .catchError((error) => print("Failed to update work: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('WORK'),
        centerTitle: true,
        backgroundColor: Colors.blue,
        elevation: 0,
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Card(
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    ListTile(
                      title: new Row(
                        children: <Widget>[
                          new Text(
                            '$dmy',
                            style: new TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 25.0),
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'TOPIC',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 20),
                        )),
                    TextFormField(
                      controller: _topicController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        setState(() {
                          topic = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'PLEASE INPUT TOPIC';
                        }
                        return null;
                      },
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'DETAIL',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 20),
                        )),
                    TextFormField(
                      controller: _detailController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        setState(() {
                          detail = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'PLEASE INPUT DETAIL';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            if (workId.isEmpty) {
                              await addWork();
                            } else {
                              await updateWork();
                            }

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NoteWidget()),
                            );
                          }
                        },
                        child: Text('SAVE'))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
