import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';

import 'package:planner/work_form.dart';
import 'package:intl/intl.dart' show DateFormat;

import 'package:shared_preferences/shared_preferences.dart';

class CalendarWidget extends StatefulWidget {
  CalendarWidget({Key? key}) : super(key: key);

  @override
  CalendarWidgetState createState() => CalendarWidgetState();
}

class CalendarWidgetState extends State<CalendarWidget> {
  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2 = DateTime.now();
  String _currentMonth = DateFormat.yMMM().format(DateTime.now());
  DateTime _targetDateTime = DateTime.now();

  int day = 0;
  int month = 0;
  int year = 0;

  final _dayController = TextEditingController();
  final _monthController = TextEditingController();
  final _yearController = TextEditingController();
  final _dmyController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadCalendar();
    _loadCalendar1();
  }

  Future<void> _loadCalendar() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      day = prefs.getInt('day') ?? 0;
      _dayController.text = '$day';
      month = prefs.getInt('month') ?? 0;
      _monthController.text = '$month';
      year = prefs.getInt('year') ?? 0;
      _yearController.text = '$year';
    });
  }

  String space = '/';
  String dmy = '';

  Future<void> _loadCalendar1() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      dmy = prefs.getString('dmy') ?? '';
      _dmyController.text = dmy;
    });
  }

  Future<void> _saveCalendar1() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('dmy', dmy);
    });
  }

  @override
  Widget build(BuildContext context) {
    final _calendarCarousel = CalendarCarousel<Event>(
      todayBorderColor: Colors.yellow.shade600,
      onDayPressed: (DateTime date, events) {
        setState(() {
          day = date.day.toInt();
          month = date.month.toInt();
          year = date.year.toInt();

          dmy = day.toString() +
              space +
              month.toString() +
              space +
              year.toString();

          {
            _saveCalendar1();
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => WorkForm(workId: '')),
            );
          }

          print('long pressed date $year ,$month,$day');

          events.forEach((event) => print(event.title));
        });
      },
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.grey,
      headerText: _currentMonth,
      headerTextStyle: TextStyle(fontSize: 20.0, color: Colors.black),
      weekFormat: false,
      height: 430.0,
      targetDateTime: _targetDateTime,
      selectedDateTime: _currentDate2,
      showIconBehindDayText: true,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomShapeBorder:
          CircleBorder(side: BorderSide(color: Colors.yellow)),
      markedDateCustomTextStyle: TextStyle(
        fontSize: 18,
        color: Colors.blue,
      ),
      todayTextStyle: TextStyle(
        color: Colors.black,
      ),
      minSelectedDate: _currentDate.subtract(Duration(days: 365)),
      maxSelectedDate: _currentDate.add(Duration(days: 365)),
      todayButtonColor: Colors.yellow,
      inactiveDaysTextStyle: TextStyle(
        color: Colors.red,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMM().format(_targetDateTime);
        });
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
      },
      markedDateMoreShowTotal: true,
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('CALENDAR'),
        centerTitle: true,
        //backgroundColor: Colors.transparent,
        backgroundColor: Colors.blue,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: 16.0,
                bottom: 16.0,
                left: 16.0,
                right: 16.0,
              ),
            ),
            Container(
              child: Card(
                margin: EdgeInsets.symmetric(horizontal: 16.0),
                child: _calendarCarousel,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
